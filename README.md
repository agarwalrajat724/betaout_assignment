# Betaout_Assignment

1)-> All the configurations are declared in application.properties file

2)-> Start the application with below command
      mvn spring-boot:run

3)-> Please give the appropriate sender user email id and password for below properties in application.properties
	  spring.mail.username=agarwalrajat724@gmail.com
      spring.mail.password=XXXXXXXXXXXXXXX

4)-> If testing with personal email id then enable "Allow less secure apps" option at Google Accounts.

5)-> For verifying send Email POST API :-
        url :- localhost:5010/email/send
      sample request body:-
      [
      	{
      		"userName": "test1",
      		"emailId": "rajat.agarwal@medlife.com",
      		"subject": "M",
      		"text": "Hello!!!!!!",
      		"mobileNumber": "9896543228"
      	},
      	{
      		"userName": "test1",
      		"emailId": "agarwalrajat724@gmail.com",
      		"subject": "M",
      		"text": "How are You ? ",
      		"mobileNumber": "9896543228"
      	}
      ]

6)-> For verifying user stats or detail GET API

       sample uri:- localhost:5010/email?emailId={email_id}


