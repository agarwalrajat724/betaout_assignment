package com.example.mongodb.demomongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class DemoMongodbApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoMongodbApplication.class, args);
	}

}
