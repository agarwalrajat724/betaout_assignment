package com.example.mongodb.demomongodb.controller;

import com.example.mongodb.demomongodb.model.Email;
import com.example.mongodb.demomongodb.model.dto.EmailDto;
import com.example.mongodb.demomongodb.response.Response;
import com.example.mongodb.demomongodb.response.UserInfoResponse;
import com.example.mongodb.demomongodb.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/email")
public class EmailController {

    private NotificationService notificationService;

    @Autowired
    public EmailController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/send")
    public ResponseEntity<Response> sendEmail(HttpServletRequest request,
                                                @RequestBody List<EmailDto> emailUsers) {
        Response response = new Response();

        if (null == emailUsers || emailUsers.isEmpty()) {
            response.setMessage("The List of Email Users passed is either NULL or EMPTY. ");
            return new ResponseEntity<Response>(response,HttpStatus.BAD_REQUEST);
        }

        notificationService.createEmail(emailUsers);

        return new ResponseEntity<Response>(response, HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.GET, value = {"","/"})
    public ResponseEntity<UserInfoResponse> getUsers(@RequestParam(value = "emailId", required = false) String emailId) {
        UserInfoResponse response = new UserInfoResponse();
        if (StringUtils.isEmpty(emailId)) {
            response.setMessage("Email Id is not passed to retrieve the information");
            return new ResponseEntity<UserInfoResponse>(response, HttpStatus.BAD_REQUEST);
        }

        List<Email> emails = notificationService.getUsersByEmailId(emailId);

        if (null != emails && !emails.isEmpty()) {
            response.setDetail(emails);
        }

        return new ResponseEntity<UserInfoResponse>(response, HttpStatus.OK);
    }


}
