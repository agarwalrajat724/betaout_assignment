package com.example.mongodb.demomongodb.controller;

import com.example.mongodb.demomongodb.model.User;
import com.example.mongodb.demomongodb.repository.UserRepository;
import com.example.mongodb.demomongodb.response.Response;
import com.example.mongodb.demomongodb.service.UserHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/assignment/user")
public class UserController {

    @Autowired
    private UserHelper userHelper;

    @Autowired
    private UserRepository userRepository;


    @RequestMapping(method = RequestMethod.POST, value = "/create")
    public ResponseEntity<Response> createUsers(HttpServletRequest request, @RequestBody List<User> users) {

        Response response = new Response();

        if (null == users || users.isEmpty()) {
            response.setMessage("The List of Users passed is either NULL or EMPTY. ");
            return new ResponseEntity<Response>(response,HttpStatus.BAD_REQUEST);
        }

        userHelper.createOrUpdateUsers(users, response);

        return new ResponseEntity<Response>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String test() {
        System.out.println("test");
        return "test";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<User> getUserById(@PathVariable("id") String id) {
        return new ResponseEntity<User>(userRepository.findOne(id), HttpStatus.OK);
    }

    @RequestMapping(value = {"","/"}, method = RequestMethod.GET)
    public ResponseEntity<List<User>> getUsers(@RequestParam(value = "email", required = false) String email,
                                               @RequestParam(value = "mobile", required = false) String mobile) {

        if (StringUtils.isEmpty(email) && StringUtils.isEmpty(mobile)) {
            return new ResponseEntity<List<User>>(userRepository.findAll(), HttpStatus.OK);
        }

        return new ResponseEntity<List<User>>(userHelper.retrieveUserByParam(email, mobile), HttpStatus.OK);
    }
}
