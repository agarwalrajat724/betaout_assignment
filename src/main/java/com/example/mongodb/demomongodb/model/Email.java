package com.example.mongodb.demomongodb.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Pattern;
import java.util.Date;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Document(collection = "email")
public class Email {

    @Id
    private String id;

    @Indexed
    private String emailId;

    private String userName;

    private String subject;

    private String message;

    private Date sentDate;

    @Pattern(regexp="(^$|[0-9]{10})")
    private String mobileNumber;

    public Email(String userName, String emailId) {
        this.userName = userName;
        this.emailId = emailId;
    }
}
