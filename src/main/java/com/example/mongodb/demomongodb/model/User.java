package com.example.mongodb.demomongodb.model;


import com.example.mongodb.demomongodb.util.Gender;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Pattern;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@CompoundIndex(name = "mobile_emailid", def = "{'mobileNumber':1,'email':1}", unique = true, background = true)
@Document(collection = "users")
public class User {

    @Id
    private String userId;

    @NotEmpty(message = "first name cannot be empty")
    //@Indexed
    private String firstName;

    //@Indexed
    private String lastName;

    //@Indexed
    // private String fullName;

    private Gender gender;

    @Email
    @NotEmpty(message = "Email can't be Empty")
    private String email;

    @Pattern(regexp="(^$|[0-9]{10})")
    private String mobileNumber;

    private boolean isActive;


//    @Value
//    static class CompositeKey implements Serializable {
//        private String mobileNumber;
//        private String email;
//    }

}
