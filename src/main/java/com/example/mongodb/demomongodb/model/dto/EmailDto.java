package com.example.mongodb.demomongodb.model.dto;

import lombok.Data;

import javax.validation.constraints.Pattern;

@Data
public class EmailDto {

    private String userName;

    private String emailId;

    private String subject;

    private String text;

    @Pattern(regexp="(^$|[0-9]{10})")
    private String mobileNumber;
}
