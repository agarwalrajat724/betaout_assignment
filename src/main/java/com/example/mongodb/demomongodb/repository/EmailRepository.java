package com.example.mongodb.demomongodb.repository;

import com.example.mongodb.demomongodb.model.Email;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface EmailRepository extends MongoRepository<Email, String> {
    List<Email> findByEmailId(String emailId);
}
