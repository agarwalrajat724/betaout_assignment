package com.example.mongodb.demomongodb.repository;

import com.example.mongodb.demomongodb.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends MongoRepository<User, String> {

    public User findByMobileNumberAndEmail(String mobileNumber, String email);

    public List<User> findByMobileNumber(String mobileNumber);

    public List<User> findByEmail(String email);
}
