package com.example.mongodb.demomongodb.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response {
    private int status;
    private String id;
    private String message;
    private Map<String, String> jsonResponse = new HashMap<String, String>();
    private Error error;


    public void addToJsonResponse(String key, String value){
        if(key != null){
            jsonResponse.put(key, value);
        }
    }

}