package com.example.mongodb.demomongodb.response;

import com.example.mongodb.demomongodb.model.Email;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserInfoResponse {

    private int status;
    private String id;
    private String message;
    private Map<String, String> jsonResponse = new HashMap<String, String>();
    private Error error;
    private List<Email> detail;

    public void addToJsonResponse(String key, String value){
        if(key != null){
            jsonResponse.put(key, value);
        }
    }
}
