package com.example.mongodb.demomongodb.service;

import com.example.mongodb.demomongodb.model.Email;
import com.example.mongodb.demomongodb.model.dto.EmailDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.mail.MessagingException;


@Component
public class EmailSender {

    private JavaMailSender mailSender;

    private static final Logger logger = LoggerFactory.getLogger(EmailSender.class);

    @Autowired
    public EmailSender(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    @Value("${spring.mail.username}")
    private String emailFrom;

    @Async
    public void send(EmailDto email) {
        logger.info("Sending email to User : " + email.getUserName() + " at email id : " + email.getEmailId());

        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(email.getEmailId());
        mail.setFrom(emailFrom);
        mail.setSubject(StringUtils.isEmpty(email.getSubject()) ? "" : email.getSubject());
        mail.setText(StringUtils.isEmpty(email.getText()) ? "" : email.getText());
        try {
            mailSender.send(mail);
            logger.info("Email Sent to : " + email.getUserName() + " at email id : " + email.getEmailId());
        } catch (MailException e) {
            logger.error("Error while sending Email to User : " + email.getUserName() + " at email id : " + email.getEmailId(), e);
        } catch (Exception e) {
            logger.error("Error while sending Email to User : " + email.getUserName() + " at email id : " + email.getEmailId(), e);
        }
    }

//    private void sendHtmlMail(Email eParams) throws MessagingException {
//        boolean isHtml = true;
//        MimeMessage message = mailSender.createMimeMessage();
//        MimeMessageHelper helper = new MimeMessageHelper(message);
//        helper.setTo(eParams.getTo().toArray(new String[eParams.getTo().size()]));
//        helper.setReplyTo(eParams.getFrom());
//        helper.setFrom(eParams.getFrom());
//        helper.setSubject(eParams.getSubject());
//        helper.setText(eParams.getMessage(), isHtml);
//        if (eParams.getCc().size() > 0) {
//            helper.setCc(eParams.getCc().toArray(new String[eParams.getCc().size()]));
//        }
//        mailSender.send(message);
//    }
//

}
