package com.example.mongodb.demomongodb.service;

import com.example.mongodb.demomongodb.model.Email;
import com.example.mongodb.demomongodb.model.dto.EmailDto;
import com.example.mongodb.demomongodb.repository.EmailRepository;
import org.apache.commons.validator.routines.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class NotificationService {


    @Autowired
    private EmailSender emailSender;

    @Autowired
    private EmailRepository emailRepository;

    private static final Logger logger = LoggerFactory.getLogger(NotificationService.class);


    private static final String MOBILE_NUMBER_REGEX = "[7-9][0-9]{9}";

    public void createEmail(List<EmailDto> emailUsers) {

        logger.info(this.getClass().getName() + " Start -> createEmail() method.");
        List<Email> emails = new ArrayList<Email>();

        for (EmailDto emailDto : emailUsers) {
            if (!StringUtils.isEmpty(emailDto.getUserName()) && !StringUtils.isEmpty(emailDto.getEmailId())
                    && EmailValidator.getInstance().isValid(emailDto.getEmailId())) {

                Email email = new Email(emailDto.getUserName(), emailDto.getEmailId());

                processEmailDto(emailDto, email);
                emails.add(email);
                emailSender.send(emailDto);
            }
        }

        if (!emails.isEmpty()) {
            emailRepository.save(emails);
        }

        logger.info(this.getClass().getName() + " End -> createEmail() method.");
    }

    private void processEmailDto(EmailDto emailDto, Email email) {

        logger.info(this.getClass().getName() + " Start -> processEmailDto() method.");

        email.setSentDate(new Date());
        if (!StringUtils.isEmpty(emailDto.getMobileNumber())
                && emailDto.getMobileNumber().matches(MOBILE_NUMBER_REGEX)) {
            email.setMobileNumber(emailDto.getMobileNumber());
        }

        if (!StringUtils.isEmpty(emailDto.getSubject())) {
            email.setSubject(emailDto.getSubject());
        }

        if (!StringUtils.isEmpty(emailDto.getText())) {
            email.setMessage(emailDto.getText());
        }

        logger.info(this.getClass().getName() + " End -> processEmailDto() method.");
    }

    public List<Email> getUsersByEmailId(String emailId) {
        return emailRepository.findByEmailId(emailId);
    }
}
