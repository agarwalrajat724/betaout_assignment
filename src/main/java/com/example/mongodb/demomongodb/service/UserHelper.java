package com.example.mongodb.demomongodb.service;

import com.example.mongodb.demomongodb.model.User;
import com.example.mongodb.demomongodb.repository.UserRepository;
import com.example.mongodb.demomongodb.response.Response;
import com.example.mongodb.demomongodb.util.Gender;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

@Service
public class UserHelper {


    @Autowired
    private UserRepository userRepository;

    private static final String MOBILE_NUMBER_REGEX = "[7-9][0-9]{9}";

    public void createOrUpdateUsers(List<User> users, Response response) {

        Iterator<User> userIterator = users.iterator();

        while (userIterator.hasNext()) {
            User user = userIterator.next();

            if (!StringUtils.isEmpty(user.getMobileNumber()) && !StringUtils.isEmpty(user.getEmail())) {
                User existingUser = userRepository.findByMobileNumberAndEmail(user.getMobileNumber(), user.getEmail());

                if (null != existingUser) {
                    updateUserDetail(existingUser, user);
                    userIterator.remove();
                } else if (user.getMobileNumber().matches(MOBILE_NUMBER_REGEX)
                        && EmailValidator.getInstance().isValid(user.getEmail())) {
                    user.setActive(Boolean.TRUE);
                    if (null != user.getGender() && !user.getGender().toString().trim().equals("")) {
                        Gender gender = Gender.valueOf(user.getGender().toString());
                        user.setGender(gender);
                    }
                }

            } else {
                userIterator.remove();
            }
        }
        if (!users.isEmpty()) {
            userRepository.save(users);
        }
    }


//    private boolean isValid(String regex, String value) {
//        Pattern pattern = Pattern.compile(regex);
//        Matcher matcher = pattern.matcher(value);
//
//        return matcher.find() && matcher.group().equals(value);
//    }

    private void updateUserDetail(User existingUser, User userUpdate) {

        if (!StringUtils.isEmpty(userUpdate.getFirstName())) {
            existingUser.setFirstName(userUpdate.getFirstName());
        }

        if (!StringUtils.isEmpty(userUpdate.getGender())) {
            existingUser.setGender(userUpdate.getGender());
        }

        if (!StringUtils.isEmpty(userUpdate.getLastName())) {
            existingUser.setLastName(userUpdate.getLastName());
        }

        existingUser.setActive(userUpdate.isActive());

        userRepository.save(existingUser);
    }

    public List<User> retrieveUserByParam(String email, String mobile) {
        List<User> users = new ArrayList<User>();
        if (!StringUtils.isEmpty(email) && !StringUtils.isEmpty(mobile)) {
            users = Arrays.asList(userRepository.findByMobileNumberAndEmail(mobile, email));
        } else if (!StringUtils.isEmpty(mobile)) {
            users = userRepository.findByMobileNumber(mobile);
        } else {
            users = userRepository.findByEmail(email);
        }
        return users;
    }
}
