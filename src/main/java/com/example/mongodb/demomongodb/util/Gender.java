package com.example.mongodb.demomongodb.util;

public enum Gender {
    M, F, O
}
